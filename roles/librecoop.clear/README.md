Deploy Role
===================

This role stops all docker-compose services and deletes their files

Role Variables
--------------

### clear

If a dictionary, possible keys: `images` (default `False`), `volumes` (default `False`), `build_cache` (default `True`), `networks` (default `True`). The keys define if it should prune it additionaly.

### workload_src_path

Path to store the docker-compose and other files. Default: `/src`.

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: librecoop.clear
      vars:
        clear:
          images: True
          volumes: True
```

License
-------

GPLv3

Author Information
------------------

librecoop https://www.librecoop.es
