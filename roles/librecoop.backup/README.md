Backup Role
===================

This role forces to make a backup with backupbot

Requirements
------------

Have librecloud deployed with `enable_backup: True`

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: librecoop.backup
```

License
-------

GPLv3

Author Information
------------------

librecoop https://www.librecoop.es
